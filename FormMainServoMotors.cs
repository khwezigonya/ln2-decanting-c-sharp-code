﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Data.OleDb;
using System.Threading;

namespace CSharpControl2013
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();      
        }

        #region DAQLite Drivers WARNING! Do not Change
        EagleDaqII.EagleDAQ DAQ = new EagleDaqII.EagleDAQ();
        string DAQName;

        string fileName = "ServoMotorData.CSV";
        bool Logging = false;
        long LogSamples=0;
        double XValue;
        int inputType = 1;                  // controls input type
        int count = 0;                      // Timer variable   

        #endregion

        // Global control variables
       

	
        byte SelectVentingValve = 4;
        byte SelectFillingValve = 1;
        byte CloseValves = 0;
        double Temperature = 7;
        double Mass = 0;
        double ShowMass = 0;
        double DesiredMass;
                     // Gain Default to one

        bool TemperatureIncreasing = false;
        bool Filling = false;
        bool Venting = true;
        
        double TemperatureGradient = 0.1;
       
        //DAQ.SendDigitalOutput(DAQName, CloseValves);
        OleDbConnection FlotationDataConnection = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|\\FlotationData.accdb;");

        private void FormLoad(object sender, EventArgs e)   // Connect to ADC/DAC
        {                        
            try 
            {
                string SerialNumber;
               
                DAQ.GetDaqName(out SerialNumber, out DAQName);
                textBoxDAQName.Text = DAQName;
                textBoxDAQSerial.Text = SerialNumber;              
            }
            catch
            {
                MessageBox.Show("uDAQ not connected", "Cannot read uDAQ",
                                       MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        
        // Interrupt for timer
        private void Timer_Tick(object sender, EventArgs e)
        {
            string WriteData = "";
            double Increment = Convert.ToDouble(timer1.Interval) / 1000;
            XValue = XValue+Increment;
            //double XValue = count * timer1.Interval;

            
            double TimeElapsed = XValue;
            textBoxTimeElapsed.Clear();
            textBoxTimeElapsed.AppendText(TimeElapsed.ToString("0.00"));
            WriteData = TimeElapsed.ToString("0.00"); // Record into text string for data logging



            //READ TEMPERATURE


            Temperature = DAQ.GetInput(DAQName, 0)/0.1168;           // Read and Setpoint @ ADC INPUT 0 
            //Temperature = Math.Round(Temperature, 1);

            try
            {

                // 

                    if (Venting & (Temperature > 35))
                    { 
                        
                    DAQ.SendDigitalOutput(DAQName, SelectVentingValve); 
                    SystemStateButton.BackColor = Color.Yellow;
                    SystemStateButton.Text = "Venting";
                    //Filling = false;
                } //SelectVentingValve = 4;

                    else if (Temperature <= 35)
                    {
                        //SelectFillingValve = 1;
                        DAQ.SendDigitalOutput(DAQName, SelectFillingValve);
                        Venting = false;
                        Filling = true;

                }

                TemperatureTextBox.Clear();
                TemperatureTextBox.AppendText(Temperature.ToString("0.0"));
                if (serialPortScale.IsOpen == false) { serialPortScale.Open(); }
            }
            catch
                { }  
         

            
            
            try
            {

                // Simulate Mass Increase
                if (Filling & (Mass < DesiredMass))
                {
                        SystemStateButton.BackColor = Color.Green;
                        SystemStateButton.Text = "Filling";
                      
                }

                else if((Mass >= DesiredMass) &(Mass>0))
                {
                    DAQ.SendDigitalOutput(DAQName, CloseValves);
                    SystemStateButton.BackColor = Color.Blue;
                    SystemStateButton.Text = "Done"; 
                    timer1.Enabled = false; 
                
                }

                MassTextBox.Clear();
                MassTextBox.AppendText(Mass.ToString("0.00"));
                
            }
            catch
            { }






            //       





            // Send output to DAQ           



         

            


            #region // FOR LOGGING DATA
            if (Logging == true)
            {
                // Write data into file 
                StreamWriter Datafile = new StreamWriter(fileName, true);
                Datafile.WriteLine(WriteData);
                Datafile.Close();
                // Show the sample count
                LogSamples++;

            }


            if (XValue > 420)
            {
                //timer1.Enabled = false;
            }

            #endregion
        }

        private void StartControl(object sender, EventArgs e)
        {

            DAQ.SendDigitalOutput(DAQName, CloseValves);
            int interval;       // Enable and set timer            
            interval = Convert.ToInt16(textBoxInterval.Text); // sampling time
            timer1.Interval = interval;
            textBoxDesiredMass.Enabled = true;
            textBoxInterval.Enabled = false;        // Disable appropriate text boxes
            count = 0;      // Reset counter

        }

        private void StopControl(object sender, EventArgs e)
        {
            DAQ.SendDigitalOutput(DAQName, CloseValves); 
            timer1.Enabled = false;     // Disable timer
            textBoxInterval.Enabled = true;
            count = 0;      // Reset counter
            XValue = 0;
            TemperatureIncreasing = false;
            Filling = false;
            Venting = false;
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            DAQ.SendDigitalOutput(DAQName, CloseValves);
            this.Close();
        }

        #region DATA LOGGING 

        private void Startlogging(object sender, EventArgs e)
        {
            int interval = timer1.Interval;
            string sInterval = interval.ToString();
            FileStream Datafile1;
            

            Logging = true;



            // Recreate Create file              
            Datafile1 = new FileStream(fileName, FileMode.Create, FileAccess.Write);
            Datafile1.Close();

            StreamWriter Datafile = new StreamWriter(fileName, true);
            Datafile.WriteLine("DATA LOGGER INTERVAL @ " + sInterval + "ms");
            Datafile.WriteLine("TIME(s),y(t)_Output,r(t)_SetPoint,Motor Velocity,u(t)_Input");
            Datafile.Close();

            // OPEN DATA BASE TO LOG DATA
            FlotationDataConnection.Open();
        }

        private void StopLogging(object sender, EventArgs e)
        {

            Logging = false;
            LogSamples = 0;


            // CLOSE DATA BASE FOR LOGGING
            FlotationDataConnection.Close();
        }

        #endregion

        #region CHART SETUP
        private void button1_Click(object sender, EventArgs e)
        {
            
        }      
        
        #endregion


        #region ACCESS DATA BASE LOGGING

        public void LogToDatabase(double TimeInSeconds, double Input )
        {
            string CommandText = "INSERT INTO FlotationDataTable ([Time_seconds],[Input]) VALUES (" +
                TimeInSeconds + "," + Input+")";

            OleDbCommand InsertCommand = new OleDbCommand(CommandText, FlotationDataConnection);
            InsertCommand.ExecuteNonQuery();
        }

        #endregion

        private void SwitchingVentValve(object sender, EventArgs e)
        {
            if (SelectVentingValve == 0)
            {
                SelectVentingValve = 4;
                DAQ.SendDigitalOutput(DAQName, SelectVentingValve);
                return;
            }
            if (SelectVentingValve == 4)
            {
                SelectVentingValve = 0;
                DAQ.SendDigitalOutput(DAQName, SelectVentingValve);
                return;
            }
        }

        private void FillingValveTest(object sender, EventArgs e)
        {
            if (SelectFillingValve == 0)
            {
                SelectFillingValve = 1;
                DAQ.SendDigitalOutput(DAQName, SelectFillingValve);
                return;
            }
            if (SelectFillingValve == 1)
            {
                SelectFillingValve = 0;
                DAQ.SendDigitalOutput(DAQName, SelectFillingValve);
                return;
            }
        }


        private void SetMass(object sender, EventArgs e)
        {
            timer1.Enabled = true;
            TemperatureIncreasing = true;
            Venting = true;
            DesiredMass = Convert.ToDouble(textBoxDesiredMass.Text);


            textBoxDesiredMass.Clear();
            textBoxDesiredMass.AppendText(DesiredMass.ToString("0.00"));
            textBoxDesiredMass.Text = DesiredMass.ToString("0.0");

        }

        byte[] SerialIndata = new byte[200];
        byte[] SerialOutdata = new byte[200];
        int NumberOfDataIn = 30;
        int NumberOfDataOut;
        int ActualDataIn;
        bool DataRecieved = false;
        bool timeout = false;
        int TotalDataRev;
        int DataTimeOut;
        #region SERIAL INTERFACE TO SCALE
        
        private void ReadInputSerialPort(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {   // Last Revised 21/12/2014 Results were excellant on the sim.           
            int TotalDataRev = 0;
            int BytesInBuffer;
            byte StartByte = 0;
            byte i = 0;
            bool BeginningFound = false;
            try
            {
                BytesInBuffer = serialPortScale.BytesToRead;


                while (BytesInBuffer < NumberOfDataIn)
                {  // Making Sure that their is the amount of data available if not pole until available
                    BytesInBuffer = serialPortScale.BytesToRead;
                }

                //TotalDataRev = serialPort1.BytesToRead;
                serialPortScale.Read(SerialIndata, 1, NumberOfDataIn);

                while(BeginningFound == false)
                { 
                    if(SerialIndata[i] == 87) { StartByte = i; BeginningFound = true; } // make sure to start at beginning
                    i++;
                }


                Mass = (SerialIndata[StartByte + 4]- 48)*10 + ( SerialIndata[StartByte + 5] - 48) + (SerialIndata[StartByte + 7] - 48)*0.1 + (SerialIndata[StartByte + 8] - 48)*0.01;

                // (SerialIndata[3] - 48)*100 + (SerialIndata[4] - 48)*10
                serialPortScale.DiscardInBuffer();
                ActualDataIn = TotalDataRev;
            }
            catch
            {
                ActualDataIn = TotalDataRev;
            }

            serialPortScale.Close();
        }
        #endregion
    }
}
